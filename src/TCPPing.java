import main.Catcher;
import main.CatcherParser;
import main.Pitcher;
import main.PitcherParser;

import java.io.IOException;
import java.security.InvalidParameterException;

public class TCPPing {

    /**
     * The main method first creates a new instance of the command line argument Parser.
     * Based on the mode selected it either starts the program in Pitcher mode or
     * Catcher mode.
     * @param args command line arguments
     */
    public static void main(String[] args) {

        /*
         * Using "contains" instead of equal comparison enables the program
         * to work with both the short (-) and long(–) dash in parameters.
         */
        String mode = args[0].contains("c") ? "c" : "p";

        try{
            switch (mode){
                // Program started in Catcher mode.
                case "c":
                    CatcherParser catcherParser = new CatcherParser();
                    catcherParser.parseArguments(args);

                    Catcher c = new Catcher(catcherParser.getPort(), catcherParser.getIpAddress());
                    c.listen();
                    break;
                // Program started in Pitcher mode.
                case "p":
                    PitcherParser pitcherParser = new PitcherParser();
                    pitcherParser.parseArguments(args);

                    Pitcher p = new Pitcher(pitcherParser.getCatcherName(), pitcherParser.getPort(), pitcherParser.getMessageSize(), pitcherParser.getMessagesPerSecond());
                    p.start();
                    break;
                default:
                    System.out.println("Error in parameters. Please specify either catcher (-c) or pitcher (-p) mode.");
                    break;
            }

        }
        catch (InvalidParameterException e) {
            System.out.println("Exception caught when parsing arguments: " + e.getMessage());
            System.exit(1);
        }
        catch (IOException e) {
            System.out.println("IO Exception caught " + e.getMessage());
            System.exit(1);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

    }

}








