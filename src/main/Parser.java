package main;

public interface Parser {
    void parseArguments(String[] arguments);
}
