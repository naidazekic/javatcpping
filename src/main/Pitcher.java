package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pitcher {

    private Socket pitcher;
    private int messageNumber = 1;
    private int responseCounter = 1;
    private int lossCounter = 0;
    private int messageSize;
    private int messagesPerSecond;
    private ArrayList<Message> messages;

    public Pitcher(String bind, int port, int messageSize, int messagesPerSecond) throws IOException {
        pitcher = new Socket(bind, port);
        this.messageSize = messageSize;
        this.messagesPerSecond = messagesPerSecond;
        messages = new ArrayList<>();
    }

    /**
     * Starts the main activities for the Pitcher: sending new messages,
     * printing out statistics, and listening for message responses.
     * @throws IOException
     */
    public void start() throws IOException {
        PrintWriter out = new PrintWriter(pitcher.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(pitcher.getInputStream()));

        /*
         * ScheduledExecutor is used to run tasks concurrently at specified intervals.
         * A new message is sent in an interval based on the amount of messages to send per second.
         * Statistics are printed out every second.
         */
        ScheduledExecutorService scheduledPool = Executors.newScheduledThreadPool(4);
        scheduledPool.scheduleAtFixedRate(() -> sendNextMessage(out), 0, convertToDelay(messagesPerSecond), TimeUnit.MILLISECONDS);
        scheduledPool.scheduleAtFixedRate(() -> printStatistics(), 0, 1, TimeUnit.SECONDS);

        /*
         * After sending and statistics are scheduled, the Pitcher starts continuously
         * listening for returning messages from Catcher.
         */
        listenForMessages(in);

    }

    /**
     * 1. Calls FN to generate content of next message
     * 2. Creates Message instance and sets time that message was sent
     * 3. Sends the message(String) to the output stream
     * 4. Adds message(Message) to the messages array
     * 5. Increments message count (used for counting total messages and labelling messages)
     *
     * @param out output stream to send message to
     */
    private void sendNextMessage(PrintWriter out) {
        String message = createNextMessage(this.messageSize);
        Message msg = new Message(messageNumber);

        msg.setTimeSent(System.currentTimeMillis());
        out.println(message);
        messages.add(msg);
        messageNumber++;
    }

    /**
     * 1. Prints statistical data
     * 2. Clears messages array after every print
     */
    private void printStatistics(){
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
        System.out.println("Time: " + sdfTime.format(new Date()));
        System.out.println("--------------------------");
        System.out.println("Number of messages sent so far: " + (messageNumber-1));
        System.out.println("Messages sent in previous second: " + messages.size());
        System.out.println("Avg time for message to return in previous second: " + Helpers.calculateAverageRTT(messages) + "ms");
        System.out.println("Max time for message to return in previous second: " + Helpers.findMaxRTT(messages) + "ms");
        System.out.println("Avg time for message to get to catcher in previous second: " + Helpers.calculateAverageTimeToCatcher(messages) + "ms");
        System.out.println("Avg time for message to get from catcher in previous second: " + Helpers.calculateAverageTimeFromCatcher(messages) + "ms");
        System.out.println("Messages lost (no response) in previous second: " + lossCounter);
        System.out.println();
        System.out.println();

        messages.clear();
    }

    /**
     * 1. Prints statistical data in a shorter format, similar to standard ping
     * 2. Clears messages array after every print
     */
    private void printShortStatistics(){
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
        System.out.println(sdfTime.format(new Date()));
        System.out.println("messages total/previous sec: " + (messageNumber-1) + "/" + messages.size() + ", messages lost: " + lossCounter);
        System.out.println("avg/max RTT: " + Helpers.calculateAverageRTT(messages) + "ms/" + Helpers.findMaxRTT(messages) + "ms; avg TTC/TFC: " + Helpers.calculateAverageTimeToCatcher(messages) + "ms/" +Helpers.calculateAverageTimeFromCatcher(messages) + "ms" );
        System.out.println();

        messages.clear();
    }

    /**
     * Reads the next line from the input stream as long as messages
     * are being received, and handles registering returned messages.
     * @throws IOException
     */
    private void listenForMessages(BufferedReader in) throws IOException{

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            String parts[] = inputLine.split(" ");
            long receivedAt = System.currentTimeMillis();

            /*
             * In case the message received does not have the expected number,
             * a warning is printed out about a missing response.
             */
            while(Integer.parseInt(parts[1]) != responseCounter){
                System.out.println("Missing response to message # " + responseCounter);
                System.out.println();
                lossCounter++;
                responseCounter++;
            }

            registerReturnedMessage(Integer.parseInt(parts[1]), receivedAt, Long.parseLong(parts[2]));
            responseCounter++;
        }
    }

    /**
     * Creates next message of specified size with the message number at the beginning.
     * Example message of size 20B: "# 23 ppppp"
     * @param msgSize the size of the message in bytes
     * @return next message to send to Catcher
     */
    private String createNextMessage(int msgSize){
        msgSize = msgSize/2;
        StringBuilder sb = new StringBuilder(msgSize);

        sb.append("# ").append(messageNumber).append(" ");

        while (sb.length() < msgSize) {
            sb.append('p');
        }

        return sb.toString();
    }

    /**
     * Calculates interval for sending messages based on how many
     * messages per second we wish to send.
     * @param messagesPerSecond the number of messages to send per second
     * @return sending interval
     */
    private long convertToDelay(int messagesPerSecond){
        double convertPeriod = (1/(double)messagesPerSecond) * 1000;
        return (long)convertPeriod;
    }

    /**
     * When the Pitcher receives a response to a message, this function finds that message in
     * the messages array based on the order number. After that, relevant fields for statistics
     * are calculated.
     * @param messageNumber which message from the Pitcher is the Catcher responding to
     * @param timeReturned time when the message returned
     * @param timeAtCatcher time when the original message from the Pitcher reached the Catcher
     */
    private void registerReturnedMessage(int messageNumber, long timeReturned, long timeAtCatcher){
        for(Message message : messages) {
            if(message.getMessageNumber() == messageNumber){
                message.setRTT(timeReturned - message.getTimeSent());
                message.setTimeToCatcher(timeAtCatcher - message.getTimeSent());
                message.setTimeFromCatcher(timeReturned - timeAtCatcher);
            }
        }


    }
}



