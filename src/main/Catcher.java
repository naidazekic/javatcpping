package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;


public class Catcher  {

    private ServerSocket catcher;

    public Catcher(int port, String ipAddress) throws IOException {
        catcher = new ServerSocket(port, 50, InetAddress.getByName(ipAddress));
    }

    /**
     * Starts waiting for message from Pitcher, once a message is received,
     * generates and sends a response to that message.
     * @throws IOException
     */
    public void listen() throws IOException {
        System.out.println("Waiting for messages on port: " + catcher.getLocalPort());
        Socket pitcher = catcher.accept();

        PrintWriter out = new PrintWriter(pitcher.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(pitcher.getInputStream()));


        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            long receivedAt = System.currentTimeMillis();
            String[] parts = inputLine.split(" ");

            /*
             * The message received from the Pitcher is i.e. "# 23 ppppp"
             * The second element in parts is the message number.
             */
            String messageNumber = parts[1];

            out.println(createResponse(inputLine.length(), messageNumber, receivedAt));
        }
    }

    /**
     * Generates a response to a specific message from the Pitcher.
     *
     * Example response: # 23 12345678 cccccc
     * The first number is the message order number, and the second is the time
     * the message reached the Catcher, which will be used back on the Pitcher
     * to generate statistics.
     *
     * @param numCharacters the length of the response to generate (based on the length of message received)
     * @param messageNumber the message order number
     * @param receivedAt time that the message was received
     * @return string which is sent as response message to Pitcher
     */
    private String createResponse(int numCharacters, String messageNumber, long receivedAt){
        StringBuilder sb = new StringBuilder(numCharacters);

        sb.append("# ").append(messageNumber).append(" ").append(receivedAt).append(" ");

        while (sb.length() < numCharacters) {
            sb.append('c');
        }

        return sb.toString();
    }


}


