package main;


/**
 * This class holds all relevant info for a single message.
 * It is mainly used in statistics calculations.
 */
public class Message {
    private int messageNumber;
    private long timeSent;
    private double timeToCatcher;
    private double timeFromCatcher;
    private double RTT;

    public Message(){}

    Message(int messageNumber){
        this.messageNumber = messageNumber;
    }

    void setTimeSent(long timeSent) {
        this.timeSent = timeSent;
    }
    long getTimeSent(){
        return this.timeSent;
    }
    int getMessageNumber() {
        return messageNumber;
    }

    double getRTT() {
        return RTT;
    }
    public void setRTT(double RTT) {
        this.RTT = RTT;
    }

    double getTimeToCatcher() {
        return timeToCatcher;
    }
    public void setTimeToCatcher(double timeToCatcher) {
        this.timeToCatcher = timeToCatcher;
    }

    double getTimeFromCatcher() {
        return timeFromCatcher;
    }
    public void setTimeFromCatcher(double timeFromCatcher) {
        this.timeFromCatcher = timeFromCatcher;
    }
}


