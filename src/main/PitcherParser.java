package main;

public class PitcherParser implements Parser {
    private int port;
    private int messagesPerSecond;
    private int messageSize = 300;
    private String catcherName;

    /**
     * Parses command line arguments in Pitcher format.
     * Sets port number, messages per second, and message size in Parser instance.
     * @param arguments command line arguments
     */
    public void parseArguments(String[] arguments){
        for (int i=1; i<arguments.length; i++) {
            if (arguments[i].contains("port")) {
                this.port = Integer.parseInt(arguments[i + 1]);
            }
            else if (arguments[i].contains("mps")) {
                this.messagesPerSecond = Integer.parseInt(arguments[i + 1]);
            }
            else if (arguments[i].contains("size")) {
                this.messageSize = Integer.parseInt(arguments[i + 1]);
            }
        }

        this.catcherName = arguments[arguments.length - 1];
    }

    public int getMessagesPerSecond() {
        return messagesPerSecond;
    }

    public int getMessageSize() {
        return messageSize;
    }

    public String getCatcherName() {
        return catcherName;
    }

    public int getPort() {
        return port;
    }
}
