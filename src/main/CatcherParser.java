package main;

public class CatcherParser implements Parser {
    private String ipAddress;
    private int port;

    /**
     * Parses command line arguments in Catcher format.
     * Sets port number, and bind IP in Parser instance.
     * @param arguments command line arguments
     */
    public void parseArguments(String[] arguments){
        for (int i=1; i<arguments.length; i++) {
            if (arguments[i].contains("port")) {
                this.port = Integer.parseInt(arguments[i + 1]);
            }
            else if (arguments[i].contains("bind")) {
                this.ipAddress = arguments[i + 1];
            }
        }
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public int getPort() {
        return port;
    }
}
