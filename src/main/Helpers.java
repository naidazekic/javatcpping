package main;
import java.util.ArrayList;

/**
 * This class is used to implement various helper methods, all of
 * which are used for statistics.
 */
public class Helpers {

    public static double calculateAverageRTT(ArrayList<Message> messages){
        double sumRTT = 0;
        for (Message message : messages){
            sumRTT += message.getRTT();
        }

        return Math.round((sumRTT/messages.size()) * 100.0) / 100.0;
    }

    public static double findMaxRTT(ArrayList<Message> messages){
        double maxRTT = 0;
        for (Message message : messages){
            if(message.getRTT() > maxRTT)
                maxRTT = message.getRTT();
        }

        return maxRTT;
    }

    public static double calculateAverageTimeToCatcher(ArrayList<Message> messages){
        double sumTTC = 0;
        for (Message message : messages){
            sumTTC += message.getTimeToCatcher();
        }

        return Math.round((sumTTC/messages.size()) * 100.0) / 100.0;
    }

    public static double calculateAverageTimeFromCatcher(ArrayList<Message> messages){
        double sumTFC = 0;
        for (Message message : messages){
            sumTFC += message.getTimeFromCatcher();
        }

        return Math.round((sumTFC/messages.size()) * 100.0) / 100.0;
    }
}


