package test;

import main.Helpers;
import main.Message;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HelpersTest {
    private static ArrayList<Message> messagesTest;

    @BeforeAll
    static void generateMessageArray(){
        messagesTest = new ArrayList<>();

        Message message1 = new Message();
        message1.setRTT(5);
        message1.setTimeToCatcher(5);
        message1.setTimeFromCatcher(5);
        messagesTest.add(message1);

        Message message2 = new Message();
        message2.setRTT(3);
        message2.setTimeToCatcher(3);
        message2.setTimeFromCatcher(3);
        messagesTest.add(message2);

        Message message3 = new Message();
        message3.setRTT(4);
        message3.setTimeToCatcher(4);
        message3.setTimeFromCatcher(4);
        messagesTest.add(message3);
    }


    @Test
    void calculateAverageRTT() {
        double averageRTT = Helpers.calculateAverageRTT(messagesTest);
        assertEquals(4, averageRTT, "Average RTT must be calculated correctly.");
    }

    @Test
    void findMaxRTT() {
        double maxRTT = Helpers.findMaxRTT(messagesTest);
        assertEquals(5, maxRTT, "Max RTT must be found correctly.");
    }

    @Test
    void calculateAverageTimeToCatcher() {
        double averageTTC = Helpers.calculateAverageTimeToCatcher(messagesTest);
        assertEquals(4, averageTTC, "Average time to catcher must be calculated correctly.");
    }

    @Test
    void calculateAverageTimeFromCatcher() {
        double averageTFC = Helpers.calculateAverageTimeFromCatcher(messagesTest);
        assertEquals(4, averageTFC, "Average time from catcher must be calculated correctly.");
    }
}