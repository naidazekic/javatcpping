package test;

import main.CatcherParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CatcherParserTest {
    private static CatcherParser catcherParser;
    private static String[] catcherArgs = {"-c", "-bind", "127.0.0.1", "-port" , "9999"};

    @BeforeAll
    static void setUp() {
        catcherParser = new CatcherParser();
        catcherParser.parseArguments(catcherArgs);
    }

    @Test
    void getIpAddress() {
        assertEquals("127.0.0.1", catcherParser.getIpAddress());
    }

    @Test
    void getPort() {
        assertEquals(9999, catcherParser.getPort());
    }
}