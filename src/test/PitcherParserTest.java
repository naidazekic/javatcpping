package test;

import main.PitcherParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PitcherParserTest {
    private static PitcherParser pitcherParser;
    private static String[] pitcherArgs = {"-p", "-port" , "9999", "-mps", "30", "-size", "1000", "kompB"};

    @BeforeAll
    static void setUp() {
        pitcherParser = new PitcherParser();
        pitcherParser.parseArguments(pitcherArgs);
    }

    @Test
    void getMessagesPerSecond() {
        assertEquals(30, pitcherParser.getMessagesPerSecond());
    }

    @Test
    void getMessageSize() {
        assertEquals(1000, pitcherParser.getMessageSize());
    }

    @Test
    void getPort() {
        assertEquals(9999, pitcherParser.getPort());
    }

    @Test
    void getCatcherName() {
        assertEquals("kompB", pitcherParser.getCatcherName());
    }

}
