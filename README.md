# TCPPing App

A Java app that implements the functionalities of the ping networking utility.

### How to run
Both Catcher and Pitcher are run as the example in the task description:

`java TCPPing -c -bind 192.168.0.1 -port 9900`

`java TCPPing -p -port 9900 -mps 30 -size 1000 kompB`

### Statistics variants

There are two modes of statistics printout. The default uses the `printStatistics()` function in `Pitcher.java`, and prints out data in 9 detailed lines. The alternative is `printShortStatistics()` which formats the output in 3 lines and is more similar to the classic ping app.

To activate the shorter output, change the function call in line 48 of `Pitcher.java` from `printStatistics()` to `printShortStatistics()`.

### Other notes

In a few cases the app did not recognize the Catcher computer name, but it will work with the IP address.